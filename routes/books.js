const express = require('express');
const router = express.Router();
const Book = require('../model')("Book");
const debug = require('debug')('lab8:books');
const path = require('path');

// get all books
router.get(/.*/, async (req, res) => {
    debug(`Sending ${path.join(__dirname, 'spa', 'dist', )}/index.html`);
    res.sendFile('index.html', { root: path.join(appRoot, 'spa', 'dist', )});
});

module.exports = router;