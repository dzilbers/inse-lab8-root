const express = require('express');
const router = express.Router();
const Book = require('../model')("Book");
const debug = require('debug')('lab8:books');

// get all books
router.get('/', async (req, res) => {
    debug("Get book/");
    try {
        let books = await Book.REQUEST();
        debug("Get all books: " + books);
        res.json(books); // return all books in JSON format
    } catch (err) {
        debug(`Error: ${err}`);
        res.send([])
    }
});

// get a book
router.get('/:id', async (req, res) => {
    debug("Get book/:id");
    try {
        let book = await Book.REQUEST(req.params.id);
        debug("Get a book: " + book);
        res.json(book); // return all todos in JSON format
    } catch (err) {
        debug(`Error: ${err}`);
        res.send(undefined)
    }
});

// create a book
router.post('/', async (req, res) => {
    debug("Post book/");
    let book = req.body;
    try {
        // create a todo, information comes from request from Angular
        book = await Book.CREATE(book);
        debug("Add a book: " + book);
        res.json(book); // return all todos in JSON format
    } catch (err) {
        debug(`Error: ${err}`);
        res.send(undefined)
    }
});

// update a book
router.put('/:id', async (req, res) => {
    debug("Put book/:id");
    let book = req.body;
    try {
        // create a todo, information comes from request from Angular
        book = await Book.UPDATE(req.params.id, book);
        debug("Update a book: " + book);
        res.json(book);
    } catch (err) {
        debug(`Error: ${err}`);
        res.send(undefined)
    }
});

// delete a book
router.delete('/:id', async (req, res) => {
    debug("Delete book/:id");
    try {
        let book = await Book.DELETE(req.params.id);
        debug("Delete a book: " + book);
        res.json(book);
    } catch (err) {
        debug(`Error: ${err}`);
        res.send(undefined)
    }
});

module.exports = router;