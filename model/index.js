const debug = require("debug")("lab8:model");
const mongo = require("mongoose");

let db = mongo.createConnection();
(async () => {
    try {
        await db.openUri('mongodb://localhost/lab8-5778');
        debug('Connected to DB');
    } catch (err) {
        debug("Error connecting to DB: " + err);
    }
})();
debug('Pending DB connection');

require("./todo")(db);
require("./book")(db);

module.exports = model => db.model(model);
